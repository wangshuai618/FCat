package com.xfdmao.fcat.coin.mapper;

import com.xfdmao.fcat.coin.entity.BtcHour;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface BtcHourMapper extends Mapper<BtcHour> {

    Boolean batchInsert(List<BtcHour> btcHourList);
}